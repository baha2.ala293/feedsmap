<html>
<head>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script >

    var map = null;
    var gmarkers = [];



function getMapData() {

  $.ajax({
    url:"/feeds/get/feeds",
    method:"GET",
    dataType:"json",
    success:function(data)
    {
      update_map(data);
    }
  });

}


update_map = function (data) {
  var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < gmarkers.length; i++) {
      gmarkers[i].setMap(null);
    }
    gmarkers = [];

    for (var i = 0, length = data.length; i < length; i++) {
      latLng = new google.maps.LatLng(data[i].location.lat, data[i].location.lng);
      bounds.extend(latLng);
      map_icon = data[i].marker_icon;

      var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        title: "<div>Location : "+data[i].city+"<div><br><div>Message: "+data[i].message+"</div><br><div> Sentiment"+data[i].sentiment+"</div>",
        icon: map_icon 
      });

      var infoWindow = new google.maps.InfoWindow();
      google.maps.event.addListener(marker, "click", function (e) {
        infoWindow.setContent(marker.title);
        infoWindow.open(map, marker);
      });
      (function (marker, data ) {
        google.maps.event.addListener(marker, "click", function (e) {
          infoWindow.setContent(marker.title +"<br>");
          infoWindow.open(map, marker);
        });
      })(marker, data[i]);
      gmarkers.push(marker);
    }
    map.fitBounds(bounds);
  };



  function initMap() {
    var mapOptions = {
      center: new google.maps.LatLng(0,0),
      zoom: 1,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      scrollwheel: true,
    };
    map = new google.maps.Map(document.getElementById("map_dev"),
      mapOptions);
    if (gmarkers.length > 0) {
      for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(map);
      }
    }
  }
getMapData();


</script>
<script 
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClpvcUyl31wMd7DJZQnnzI006S99u9nnM&libraries=places&callback=initMap"
async defer>
</script>
</head>
<body>
  <div id="map_dev" style="width: 1000px; height: 500px; "></div>
  <div>
    <h3>Sentiments Color Codes</h3>
    <ul>
        <li style="margin: 10px 0;">Negative: Red <i><img src="http://maps.google.com/mapfiles/ms/micons/red-dot.png" alt=""></i></li>
        <li style="margin: 10px 0;">Positive : Green <i><img src="http://maps.google.com/mapfiles/ms/micons/green-dot.png" alt=""></i></li>
        <li style="margin: 10px 0;">Normal: Blue <i><img src="http://maps.google.com/mapfiles/ms/micons/blue-dot.png" alt=""></i></li>
    </ul>
  </div>
</body>
</html>