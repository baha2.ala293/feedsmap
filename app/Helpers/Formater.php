<?php
namespace App\Helpers;


class Formater
{
    public $sendRequest;
    public $parser;

    public function __construct(SendRequest $sendRequest, Parser $parser)
    {
        $this->sendRequest = $sendRequest;
        $this->parser      = $parser;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    private function getData()
    {
        return $this->data;
    }
    

    public function formateFeeds()
    {
        $feeds = $this->getData()['feed']['entry'];
        return $this->getFeedsMessages($feeds);
    }

    private function getFeedsMessages($feeds)
    {
        $messages = [];
        foreach ($feeds as $key => $feed) {
            $message = $feed['content']['$t'];
            $messages[$key]['message']      = $this->getMessageText($message);
            $messages[$key]['city']         = $this->getCityName($message);
            $messages[$key]['sentiment']    = $this->getSentiment($message);
            $messages[$key]['location']     = $this->getCityLocation($this->getCityName($message));
            $messages[$key]['marker_icon']  = $this->getMapMarker($this->getSentiment($message));
        }
        return $messages;
    }


    public function getMessageText($message)
    {
        preg_match('/message:(?<messageText>.*?), sentiment:/',$message,$messageMatch);
        return $messageMatch['messageText'];
    }
    public function getSentiment($message)
    {
        preg_match('/sentiment:.*?(?<sentiment>\w+)/',$message,$sentiment);
        return $sentiment['sentiment'];
    }

    public function getCityName($message)
    {
        preg_match('/([A-Z][a-z]*), (?:([A-Z][a-z]*),\s+)?(?:([A-Z][a-z]*),\s+)?sentiment: (?:Negative|Positive|Neutral)| (?:from|in|at the) ([A-Z][a-z]*) | (?!Life)([A-Z][a-z]*) is/',$message,$locationMatch);
        unset($locationMatch[0]);
        $location = array_values(array_filter($locationMatch));
        return $location[0];
    }

    public function getCityLocation($city)
    {
        $feedsUrl = "https://maps.googleapis.com/maps/api/geocode/json?address={$city}&key=AIzaSyClpvcUyl31wMd7DJZQnnzI006S99u9nnM";
        $this->sendRequest->setUrl($feedsUrl);
        $this->parser->setData($this->sendRequest->sendGetRequest());
        return $this->parser->parseJson()['results'][0]['geometry']['viewport']['northeast'];
    }

    public function getMapMarker($sentiment)
    {
        $base_url = "http://maps.google.com/mapfiles/ms/micons/";
        return strtr(strtolower($sentiment), ['negative' => $base_url.'red-dot.png','neutral' => $base_url.'blue-dot.png','positive' => $base_url.'green-dot.png']);
    }



}