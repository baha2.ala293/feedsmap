<?php

namespace App\Http\Controllers;

use App\Helpers\Formater;
use App\Helpers\Parser;
use App\Helpers\SendRequest;
use Illuminate\Http\Request;

class VisualizeMessagesController extends Controller
{
    
    public $sendRequest;
    public $parser;
    public $formater;

    function __construct(SendRequest $sendRequest, Parser $parser,Formater $formater)
    {
        $this->sendRequest = $sendRequest;
        $this->parser      = $parser;
        $this->formater    = $formater;
    }


    public function index()
    {
        return view('map',compact('feeds'));
    }

    public function getFeeds()
    {
         $feedsUrl = "https://spreadsheets.google.com/feeds/list/0Ai2EnLApq68edEVRNU0xdW9QX1BqQXhHRl9sWDNfQXc/od6/public/basic?alt=json";
        $this->sendRequest->setUrl($feedsUrl);
        $this->parser->setData($this->sendRequest->sendGetRequest());
        $this->formater->setData($this->parser->parseJson());
        $feeds = $this->formater->formateFeeds();
        return response()->json($feeds);
    }



}
