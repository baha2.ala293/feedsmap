<?php

namespace App\Helpers;


class Parser
{
    public $data;


    public function parseJson()
    {
        return json_decode($this->data,true);
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    private function getData()
    {
        return $this->data;
    }


}