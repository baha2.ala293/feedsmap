<?php
namespace App\Helpers;


/**
* 
*/
class SendRequest
{
    public $url;


    public function setUrl($url)
    {
        $this->url = $url;
    }

    private function getUrl()
    {
        return $this->url;
    }


    public function sendGetRequest()
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->getUrl(),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET"));

        try {
            $response = curl_exec($curl);
            return $response;
        } catch (Exception $e) {
            $err = curl_error($curl);
            return $err;
        }
        curl_close($curl);
    }



}
